import React,{ Suspense } from 'react';
// import Table from './components/Table';
import Navbar from './components/Navbar';
import Chart from './components/Chart';


const Table = React.lazy(() => import('./components/Table'))

const App = () => {
  return (
    <div>
      <Navbar />
      <div style={{width: "100%" ,display:"flex",justifyContent:"center"}}>
        <Chart />
      </div>
      <div style={{width: "100%",display:"flex",justifyContent:"center"}}>
        <Suspense fallback={<h1 style={{fontSize: "40px"}}>你要很仔細才看的到我</h1>}>
          <Table />
        </Suspense>
      </div>
    </div>
  );
}

export default App;
